package {
	import com.bit101.components.PushButton;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class LineDrawingAlgo extends Sprite {
		private static const CELL_SIZE:uint = 50; 
		
		private var _grid:Sprite;
		private var _canvas:Sprite;
		
		private var _numCols:uint = 0;
		private var _numRows:uint = 0;
		
		private var _start:Point;
		private var _end:Point;
		
		public function LineDrawingAlgo() {
			addEventListener(Event.ADDED_TO_STAGE, addedToStageEventHandler);
		};
		
		private function addedToStageEventHandler(pEvent:Event):void {
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align     = StageAlign.TOP_LEFT;
			
			drawGrid();
			
			var buttonNaive:PushButton = new PushButton(this, 10, 0, "Naive", 
														naiveClickedEventHandler);
			
			var buttonBresenham:PushButton = new PushButton(this, 115, 0, "Bresenham", 
														    bresenhamClickedEventHandler);
				
			var buttonDDA:PushButton = new PushButton(this, 220, 0, "DDA Lines", 
													  DDAClickedEventHandler);
			
			_canvas = addChild(new Sprite()) as Sprite;
			
			_canvas.x = CELL_SIZE / 2;
			_canvas.y = CELL_SIZE / 2;
			
			addChild(_canvas);
			
			_grid.addEventListener(MouseEvent.CLICK, stageClickEventHandler);
		};
		
		private function stageClickEventHandler(pEvent:MouseEvent):void {
			if (!_start) {
				_start = convertFromGlobalToIndex(stage.mouseX, stage.mouseY);
				drawPoint(_start.x, _start.y, 0xFF0000);
				
				return;
			}
			
			if (!_end) {
				_end = convertFromGlobalToIndex(stage.mouseX, stage.mouseY);

				drawPoint(_end.x, _end.y, 0x00FF00);
				
				return;
			}
			
			if (_start && _end) {
				_start = null;
				_end   = null;
				
				cleanCanvas();
			}
		};
		
		private function convertFromGlobalToIndex(pX:Number, pY:Number):Point {
			return new Point(uint((pX - CELL_SIZE / 2) / CELL_SIZE), uint((pY - CELL_SIZE / 2) / CELL_SIZE));
		};
		
		private function naiveClickedEventHandler(pEvent:Event):void {
			cleanCanvas();
			
			drawPoint(_start.x, _start.y, 0xFF0000);
			drawPoint(_end.x, _end.y, 0x00FF00);
			
			drawNaive(_start.x, _start.y, _end.x, _end.y);
		};
		
		private function bresenhamClickedEventHandler(pEvent:Event):void {
			cleanCanvas();
			
			drawPoint(_start.x, _start.y, 0xFF0000);
			drawPoint(_end.x, _end.y, 0x00FF00);
			
			drawBresenham(_start.x, _start.y, _end.x, _end.y);
		};
		
		private function DDAClickedEventHandler(pEvent:Event):void {
			cleanCanvas();
			
			drawPoint(_start.x, _start.y, 0xFF0000);
			drawPoint(_end.x, _end.y, 0x00FF00);
			
			drawDDALines(_start.x, _start.y, _end.x, _end.y);
		};
		
		private function cleanCanvas():void {
			_canvas.graphics.clear();
		};
		
		private function drawGrid():void {
			_numCols = (stage.stageWidth  / CELL_SIZE) - 1;
			_numRows = (stage.stageHeight / CELL_SIZE) - 1;
			
			_grid = addChild(new Sprite()) as Sprite;
			
			_grid.x = CELL_SIZE / 2;
			_grid.y = CELL_SIZE / 2;
			
			_grid.graphics.clear();			
			_grid.graphics.lineStyle(1, 0x0);
			
			_grid.graphics.beginFill(0xFFFFFF);
			_grid.graphics.drawRect(0, 0, CELL_SIZE * _numCols, CELL_SIZE * _numRows);
			_grid.graphics.endFill();
			
			for (var col:uint = 0; col < _numCols + 1; col++) {
				for (var row:uint = 0; row < _numRows + 1; row++) {
					_grid.graphics.moveTo(col * CELL_SIZE, 0);
					_grid.graphics.lineTo(col * CELL_SIZE, CELL_SIZE * _numRows);
					_grid.graphics.moveTo(0, row * CELL_SIZE);
					_grid.graphics.lineTo(CELL_SIZE * _numCols, row * CELL_SIZE);
				}
			}
		};
		
		private function drawNaive(startX:uint, startY:uint, endX:uint, endY:uint):void {			
			var dx:int = endX - startX;
			var dy:int = endY - startY;
			
			for (var nextX:uint = startX; nextX < endX; nextX++) {
				var nextY:uint = startY + dy * (nextX - startX) / dx;
				
				if (nextX == startX && nextY == startY) {
					continue;
				}
				
				drawPoint(nextX, nextY, 0xCCCCCC);
			}
		};
		
		private function drawBresenham(startX:int, startY:int, endX:int, endY:int):void {
			var dx:int = endX - startX;
			var dy:int = endY - startY;
			
			var error:Number = 0;
			var dErr:Number  = Math.abs(dy / dx); 
			
			var y:int = startY;
			
			for (var x:int = startX; x < endX; x++) {
				if (x != startX && y != startY) {
					drawPoint(x, y, 0xCCCCCC);	
				}
				
				error = error + dErr;
				
				if (error >= 0.5) {
					y     = y + 1;
					error = error - 1.0;	
				}
			}
		};
		
		private function drawDDALines(startX:int, startY:int, endX:int, endY:int):void {
			var x:Array = [];
			var y:Array = [];
			
			var dx:Number = endX - startX;
			var dy:Number = endY - startY;
			
			var L:int = Math.max(Math.abs(dx), Math.abs(dy));
			
			dx /= L;
			dy /= L;
			
			var i:int = 0;
			
			x[i] = startX;
			y[i] = startY;
			
			i++;
			
			while (i < L) {
				x[i] = x[i-1] + dx;
				y[i] = y[i-1] + dy;
				i++;
			}
			
			x[i] = endX;
			y[i] = endY;
			
			i = 1;
			
			while (i <= L - 1) {
				drawPoint(x[i], y[i], 0xCCCCCC);
				i++;
			}
		};
		
		private function drawPoint(pX:uint, pY:uint, pColor:uint):void {
			_canvas.graphics.beginFill(pColor);
			_canvas.graphics.drawRect(pX * CELL_SIZE, pY * CELL_SIZE, CELL_SIZE, CELL_SIZE);
			_canvas.graphics.endFill();
		};
	};
}